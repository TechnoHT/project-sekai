import os
import sys
import json

import api.crypt
import api.binary


def decrypt(name):
    with open(name, 'rb') as f:
        a = f.read()

    a = api.crypt.decrypt(a)
    a = api.binary.deserialize(a)
    return a


def main():
    assert len(sys.argv) > 1
    json.dump(decrypt(sys.argv[1]), sys.stdout, indent=4, ensure_ascii=False)


if __name__ == '__main__':
    main()
