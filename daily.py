import api
import main


def function(a):
    try:
        sekai = api.Sekai.from_credential(a['userID'], a['credential'])
    except AssertionError:
        return None

    sekai.login_bonus()

    # sekai.presents()
    # sekai.exchange_gacha_ceil_item()
    return api.update(sekai, a)


if __name__ == '__main__':
    main.run(function, n_threads=32)
