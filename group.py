import json
import random
import argparse
import functools


import api


@functools.lru_cache()
def get_card_ids(unit):
    '''
    unit: light_sound, idol, street, theme_park, school_refusal, piapro
    '''

    cards = api.database('cards', key='id')
    characters = api.database('gameCharacters', key='id')
    card_ids = []
    for card in cards.values():
        character_id = card['characterId']
        character = characters[character_id]
        if character['unit'] == unit:
            card_ids.append(card['id'])
    return card_ids


def up(account, card_ids: list) -> float:
    n = min(len(account['cards']['4'].values()), len(card_ids))
    m = sum(x in account['cards']['4'].values() for x in card_ids)
    return m / n


conditions = {
    # 'a': lambda a: a['userID'] % 1000000 in [
    #     559240
    # ],

    # 'in': lambda a: True,

    # 'limit': lambda a: list(a['cards']['4'].values()) == [185, 186, 187],

    **{
        'pool/auto/%s' % star4: lambda a, star4=star4: (
            len(a['cards']['4']) == star4 and
            len(a['cards']['3']) >= 10 and
            up(a, get_card_ids('light_sound')) <= 0.4 and
            up(a, get_card_ids('idol')) <= 0.4 and
            up(a, get_card_ids('street')) <= 0.4 and
            up(a, get_card_ids('theme_park')) <= 0.4 and
            up(a, get_card_ids('school_refusal')) <= 0.4 and
            a['gem'] >= 15000 and
            len(groups['pool/auto/%s' % star4]) < 10
        )
        for star4 in [3, 4, 5, 6, 7, 8]
    },

    # 'up': lambda a: (
    #     # up(a, [get_card_ids('street')]) > 0.3 and a['gem'] >= 3000
    #     (212 in a['cards']['4'].values() or 213 in a['cards']['4'].values()) and 175 in a['cards']['4'].values()
    # ),

    **{
        'pool/%s' % star4: lambda a, star4=star4: len(a['cards']['4']) >= star4
        for star4 in range(15, 0, -1)
    },

    # ** {
    #     'gem/%s' % amount: lambda a, amount=amount: a['gem'] >= amount
    #     for amount in range(15000, 0, -1000)
    # },

    # 'fes': lambda a: random.random() < 0.9,

    # 'others': lambda a: True,
}

groups = {
    group: []
    for group in conditions
}


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('input', nargs='+')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse()

    accounts = {}
    for i in args.input:
        with open(i, encoding='utf-8') as f:
            for account in json.load(f):
                if account['userID'] in accounts:
                    if accounts[account['userID']]['gachaTimes'] < account['gachaTimes']:
                        accounts[account['userID']] = account
                else:
                    accounts[account['userID']] = account

    for account in accounts.values():
        for group, condition in conditions.items():
            if condition(account):
                groups[group].append(account)
                break

    for group, accounts in groups.items():
        accounts.sort(key=lambda a: a['userID'])
        print(group, len(accounts))
        with open('%s.json' % group, 'w', encoding='utf-8') as f:
            json.dump(accounts, f, indent=4, ensure_ascii=False)
