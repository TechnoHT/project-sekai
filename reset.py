import random

import api
import main


def function(a):
    try:
        sekai = api.Sekai.from_credential(a['userID'], a['credential'])
    except AssertionError:
        return None

    sekai.suite()
    inherit_password = ''.join([
        '%s%03d' % ('ABCD'[i], random.randrange(10**3))
        for i in range(4)
    ])
    inherit_id = sekai.inherit(inherit_password)

    return api.update(sekai, {
        'userID': sekai.user_id,
        'credential': sekai.credential,
        'inheritID': inherit_id,
        'inheritPassword': inherit_password,
    })


if __name__ == '__main__':
    main.run(function, n_threads=32)
