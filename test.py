
import api
import main
import time


def function(a):
    time.sleep(0.001)


if __name__ == '__main__':
    main.run(function)

# r = sekai('GET', '/api/suite/user/{user_id}')


# for i in range(1):
#     r = sekai('POST', '/api/user/{user_id}/live', {
#         'musicId': 1,
#         'musicDifficultyId': 4,
#         'musicVocalId': 1,
#         'deckId': 1,
#         'boostCount': 5,
#         'isAuto': True,
#     })
#     user_live_id = r['userLiveId']
#     print('user_live_id:', user_live_id)

#     time.sleep(30)

#     r = sekai('GET', '/api/system')

#     r = sekai('PUT', '/api/user/{user_id}/live/%s' % user_live_id, {
#         'score': 200000,
#         'perfectCount': 0,
#         'greatCount': 0,
#         'goodCount': 0,
#         'badCount': 0,
#         'missCount': 0,
#         'maxCombo': 0,
#         'life': 1000,
#         'tapCount': 0,
#         'continueCount': 0,
#     })
#     if 'errorMessage' in r:
#         print(r)
#         break

# present_ids = [present['presentId'] for present in r['userPresents']]
# r = sekai('POST', '/api/user/{user_id}/present', {
#     'presentIds': present_ids,
# })


# with open('master/actionSets.json') as f:
#     action_sets = json.load(f)

# for action_set in action_sets:
#     r = sekai('PUT', '/api/user/{user_id}/action-set/%s' % action_set['id'])
#     print(str(r)[:80])
#     break

# with open('r.json', 'w') as f:
#     json.dump(r, f, ensure_ascii=False, indent=4)
