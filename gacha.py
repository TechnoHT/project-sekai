import api
import main


def function(a):
    # if a['gem'] < 6000:
    #     return a

    try:
        sekai = api.Sekai.from_credential(a['userID'], a['credential'])
    except AssertionError:
        return None

    sekai.suite()
    a = api.update(sekai, a)

    try:
        star4_before = len(a['cards']['4'])

        while a['gem'] >= 3000:
            a['gem'] -= 3000
            sekai.gacha(78, 143)

        sekai.card_exchange()
        # sekai.exchange_gacha_ceil_item()
        a = api.update(sekai, a)

        star4_after = len(a['cards']['4'])
        if star4_after - star4_before >= 1:
            with main.lock:
                print(a['userID'], star4_before, '->', star4_after, '(+%s)' % (star4_after - star4_before))

    except Exception as e:
        print(e)

        try:
            sekai = api.Sekai.from_credential(a['userID'], a['credential'])
            sekai.suite()
            a = api.update(sekai, a)
        except:
            return None

    return a


if __name__ == '__main__':
    main.run(function)
