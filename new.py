import random

import api
import main


def function(a):
    sekai = api.Sekai.new()
    sekai.login_bonus()
    sekai.presents()

    sekai.gacha(78, 143)

    a = api.update(sekai, {
        'userID': sekai.user_id,
        'credential': sekai.credential,
    })

    if len(a['cards']['4']) < 3:
        return

    with main.lock:
        print(a['userID'], len(a['cards']['4']))
        for name in a['cards']['4']:
            print(name)

    inherit_password = ''.join([
        '%s%03d' % ('ABCD'[i], random.randrange(10**3))
        for i in range(4)
    ])
    inherit_id = sekai.inherit(inherit_password)
    sekai.tutorials()
    sekai.beginner_missions()
    sekai.presents()

    sekai.card_exchange()
    # sekai.exchange_gacha_ceil_item()

    return api.update(sekai, {
        'userID': sekai.user_id,
        'credential': sekai.credential,
        'inheritID': inherit_id,
        'inheritPassword': inherit_password,
    })


if __name__ == '__main__':
    main.run(function, 500000, 'new.out.json', n_threads=32)
