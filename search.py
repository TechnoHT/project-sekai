import json
import random
import argparse

from group import get_card_ids, up


def function(a):
    if len(a['cards']['4']) < args.star4:
        return

    if len(a['cards']['3']) < args.star3:
        return

    if a['gem'] < args.gem:
        return

    if args.id and ('000000' + args.id)[-6:] != str(a['userID'])[-6:]:
        return

    # if a['materials'].get('16', 0) < 5:
    #     return

    # if 116 not in a['cards']['4'].values():
    #     return
    # if 152 not in a['cards']['4'].values():
    #     return
    # if 160 not in a['cards']['4'].values():  # 192
    #     return

    characters = [
        *a['cards']['4'].keys(),
        *a['cards']['3'].keys(),
    ]

    for character in args.character:
        flag = False
        for character0 in characters:
            if character in character0:
                flag = True
                break
        if not flag:
            return

    return True


def print_account(a):
    print('%s, ★4=%d+%d, ★3=%d, jewel=%d' % (
        str(a['userID'])[-6:],
        len(a['cards']['4']), a['materials'].get('16', 0),
        len(a['cards']['3']),
        a['gem'],
    ))

    for character in a['cards']['4']:
        print(character)
    # for character in a['cards']['3']:
    #     print(character)

    print('引き継ぎID :', a['inheritID'])
    print('パスワード :', a['inheritPassword'])
    print('======')


def parse():
    parser = argparse.ArgumentParser()

    parser.add_argument('-3', dest='star3', type=int, default=0)
    parser.add_argument('-4', dest='star4', type=int, default=0)
    parser.add_argument('--gem', dest='gem', type=int, default=0)
    parser.add_argument('--id', dest='id', type=str, default='')
    parser.add_argument('--count', dest='count', action='store_true')
    parser.add_argument('--character', dest='character', nargs='*', default=[])
    parser.add_argument('input', type=str, nargs='+')

    return parser.parse_args()


if __name__ == '__main__':
    args = parse()

    input_accounts = []
    output_accounts = []

    for i in args.input:
        with open(i, encoding='utf-8') as f:
            input_accounts += json.load(f)

    random.shuffle(input_accounts)

    for account in input_accounts:
        if function(account):
            output_accounts.append(account)

    if not args.count:
        for account in output_accounts[:30]:
            print_account(account)

    print('count =', len(output_accounts))
