import json
import time
import tqdm
import queue
import threading
import traceback
import collections


lock = threading.Lock()


def thread(function, input_accounts: queue.Queue, output_accounts: queue.Queue, progress: tqdm.tqdm):
    while True:
        try:
            account = input_accounts.get_nowait()
        except queue.Empty:
            return
        except AttributeError:
            account = {}

        try:
            account = function(account)
            if account:
                output_accounts.put(account)
        except Exception as e:
            print(repr(e))
            input_accounts.put(account)

        with lock:
            progress.update()


def get_accounts_from_file(file_name):
    accounts = queue.Queue()
    with open(file_name, encoding='utf-8') as f:
        for account in json.load(f):
            accounts.put(account)

    return accounts


def get_accounts_from_iterable(iterable):
    accounts = queue.Queue()
    for account in iterable:
        accounts.put(account)

    return accounts


def run(function, input_file='in.json', output_file='out.json', n_threads=32):
    if isinstance(input_file, str):
        input_accounts = get_accounts_from_file(input_file)
    elif isinstance(input_file, collections.Iterable):
        input_accounts = get_accounts_from_iterable(input_file)
    elif isinstance(input_file, int):
        input_accounts = get_accounts_from_iterable((None for _ in range(input_file)))

    threads = []
    progress = tqdm.tqdm(total=input_accounts.qsize(), dynamic_ncols=True)
    output_accounts = queue.Queue()
    active_count = threading.active_count()
    for _ in range(n_threads):
        t = threading.Thread(target=thread, args=(function, input_accounts, output_accounts, progress), daemon=True)
        t.start()
        threads.append(t)
    try:
        while threading.active_count() > active_count:  # tqdm use 1 thread
            time.sleep(1)
    except KeyboardInterrupt:
        pass

    accounts = []
    while True:
        try:
            account = input_accounts.get_nowait()
            if account:
                accounts.append(account)
        except queue.Empty:
            break

    print('Waiting for threadings..')
    try:
        while threading.active_count() > active_count:  # tqdm use 1 thread
            time.sleep(1)
    except KeyboardInterrupt:
        pass

    while True:
        try:
            account = output_accounts.get_nowait()
            if account:
                accounts.append(account)
        except queue.Empty:
            break

    if output_file and accounts:
        with open(output_file, 'w', encoding='utf-8') as f:
            json.dump(accounts, f, indent=4, ensure_ascii=False)

    return accounts
